import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

// - - - servicio
import { CommonService } from './common.service';

//- - - - Importar las rutas
import { ROUTES } from './common.route';

// - - - providers
import { Data } from "./providers/data";

import { NavComponent } from './components/nav/nav.component';
import { MainComponent } from './components/main/main.component';
import { PropiedadComponent } from './components/propiedad/propiedad.component';

// - - - angular maps
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    MainComponent,
    PropiedadComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot( ROUTES, {useHash: false} ),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAgZjaAFfLwZPTBRrNkKmfwE35xL1kmyms'
    })
  ],
  providers: [
    CommonService,
    Data
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

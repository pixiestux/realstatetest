import { Component, OnInit } from '@angular/core';

import { CommonService } from '../../common.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-propiedad',
  templateUrl: './propiedad.component.html',
  styleUrls: ['./propiedad.component.css']
})
export class PropiedadComponent implements OnInit {

  propiedades: any[] = [];
  propiedadSeleccionada : any[] = [];
  idPropiedad:number = null;


  constructor( private service:CommonService,
               private router: ActivatedRoute ) {
    this.service.getProperties()
      .subscribe( (respuesta:any) => {
        this.propiedades = respuesta;
        console.log(this.propiedades);
        this.router.params.subscribe(paramsUrl => {
          this.idPropiedad = paramsUrl['id'];
          // console.log( this.idPropiedad  );
          let array = this.propiedades;
          // - - -Busco el ID en el arbol de categorias
          const resultado = array.find( item => item.id == this.idPropiedad );
          this.propiedadSeleccionada = resultado;
        });

      });
  }

  ngOnInit() {
  }

}

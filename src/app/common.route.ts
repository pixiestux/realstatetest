import { Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';
import { PropiedadComponent } from './components/propiedad/propiedad.component';



export const ROUTES: Routes = [
	{path: 'home', component: MainComponent},
	{path: 'propiedad/:id', component: PropiedadComponent},
	{path: '', pathMatch: 'full', redirectTo: 'home'},
	{path: '**', pathMatch: 'full', redirectTo: 'home'}
];
